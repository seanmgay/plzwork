#!/bin/bash


# faster spin AKA reducing the sleep time but keeping everything else the same
echo -ne '/';
sleep 0.1;
echo -ne '\b-';
sleep 0.1;
echo -ne '\b\\'
sleep 0.1;
echo -ne '\b|';
sleep 0.1;
echo -ne '\b/';
sleep 0.1;
echo -ne '\b-';
sleep 0.1;
echo -ne '\b\\';
sleep 0.1;
echo -ne '\b|';
echo -ne "\b \n";
