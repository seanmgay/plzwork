#!/bin/bash

#For loop 10x

for i in {1..10} #will do what is below 10x
do
     echo -ne '/';
     sleep 1;
     echo -ne '\b-';
     sleep 1;
     echo -ne '\b\\'
     sleep 1;
     echo -ne '\b|';
     sleep 1;
     echo -ne '\b/';
     sleep 1;
     echo -ne '\b-';
     sleep 1;
     echo -ne '\b\\';
     sleep 1;
     echo -ne '\b|';
     echo -ne "\b";
done
