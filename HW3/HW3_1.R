#longitudinal data script (works well)
options(stringsAsFactors = FALSE)
setwd("/users/redsk/Desktop/data")

pdf('/Users/redsk/Desktop/HW3graph.pdf')
#making each txt file a variable
for (donor in 1:100){
  print(donor)
  if (donor < 10){
  donorfileID <- paste0("donor00", donor);
  }else if (donor < 100) {
  donorfileID <- paste0("donor0", donor);
  } else if (donor < 1000) {
  donorfileID <- paste0("donor", donor)
  }
  empty <- matrix(0, nrow = 5, ncol = 10)
  for (tp in 1:10){
    if (tp < 10){
    tpfileID <- paste0("tp00", tp);
  }else if (tp<100){
    tpfileID <- paste0("tp0", tp)};
    file <- paste0(donorfileID,"_", tpfileID, ".txt")
    
    onetp <- read.csv(paste0("/Users/redsk/Desktop/data/", file))
    
    empty[,tp] <- onetp$data
  }
  plot(rep(1:10, each=5), as.vector(empty), xlab="time point", ylab= "phenotype") #plotting the data
  }
dev.off()