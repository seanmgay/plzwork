options(stringsAsFactors = FALSE)
library(tidyverse)
library(stringr) 
setwd("C:/Users/Sean/Documents/Neuroanalytics class")


## create variable to have files

fped <- 'hapmap1.ped'
fphe <- 'qt.phe'
fmap <- 'hapmap1.map'

## output variables
fsnp <-  'highmissingsnps.txt'
fpedout <- 'hapmap1_nomissing.ped'
fmapout <- 'hapmap1_nomissing.map'
flowafsnps <- 'lowafsnps.txt'
flowafmap <- 'hapmap1_nomissing_AF.map'
flowafped <- 'hapmap_nomissing_AF.ped'
fpdf <- 'phnoxgeno.pdf'
## Reads in fped variable that contains hapmap.ped file takes a long time
ped <- read.table(fped)


##eliminates unneccessary coumns (the first 6) and assigns to new variable

tmpSNPs <- ped[,c(7:ncol(ped))];

nSNPs <- ncol(tmpSNPs)/2

ndonors <- nrow(ped)

cat('the total number od SNPs is:', nSNPs, '\n');
cat('the total number of donors is:', ndonors, '\n')

SNPs <- matrix(0,nrow=ndonors,ncol=nSNPs);

for (i in 1:nSNPs){
  
  snprecode <- matrix(NA, nrow=ndonors,ncol=1);
  onesnp <- tmpSNPs[,c(2*i-1,2*i)];
  #classify the snps 
  NAind <- which(onesnp[,1]==0 & onesnp[,2]==0);
  homlind <- which(onesnp[,1]==1 & onesnp[,2]==1);
  hetlind <- which((onesnp[,1]==1 & onesnp[,2]==2) | (onesnp[,1]==2 & onesnp[,2]==1));
  hom2ind <- which(onesnp[,1]==2 & onesnp[,2]==2)
  
  
  snprecode[NAind] <- NA;
  snprecode[homlind] <- 0;
  snprecode[hetlind] <- 1;
  snprecode[hom2ind] <- 2;
  
  SNPs[,i] <- snprecode;
    }


ped.noms = cbind(ped[,1:6],pedSNPs.noms);
map <- read.table(fmap);
colnames(SNPs) <- map$V2;
rownames(SNPs) <- ped[,1]

missrate = colSums(is.na(SNPs))/ndonors;
missingind = which(missrate>0.05);
#save list of missing SNPs
missingrs <- colnames(SNPs)[missingind];
write.table(missingrs, file=fsnp,quote=FALSE,row.names=FALSE,col.names=FALSE);
#save .map of nomissing
map.noms<-map[-missingind,];
write.table(map.noms,file=fmapout,quote=FALSE,row.names=FALSE,col.names=FALSE);
SNPs.noms <- SNPs[,missingind];
pedSNPs.noms<-matrix(NA,nrow=ndonors,ncol=(2*ncol(SNPs.noms)));
#denote if SNP is NA, hom1, het, or hom2
for (i in 1:ncol(SNPs.noms)) {
  NAind = which(is.na(SNPs.noms[,i]));
  hom1ind = which(SNPs.noms[,i]==0);
  hetind = which(SNPs.noms[,i]==1);
  hom2ind <- which(SNPs.noms[,i]==2);
  
  newpedind1 = i*2-1;
  newpedind2 = i*2;
  #assign allele number
  pedSNPs.noms[NAind,newpedind1] = 0;
  pedSNPs.noms[NAind,newpedind2] = 0;
  pedSNPs.noms[hom1ind,newpedind1] = 1;
  pedSNPs.noms[hom1ind,newpedind2] = 1;
  pedSNPs.noms[hetind,newpedind1] = 1;
  pedSNPs.noms[hetind,newpedind2] = 2;
  pedSNPs.noms[hom2ind,newpedind1] = 2;
}
#bind to matrix with original ped 1:6 columns
ped.noms = cbind(ped[,1:6],pedSNPs.noms);
#save nomissing .ped
write.table(ped.noms,file=fpedout,quote=FALSE,row.names=FALSE,col.names=FALSE);
#sum all columns not missing
nomissingdonors <- colSums(!is.na(SNPs.noms));
#find allele frequency
AF <- colSums(SNPs.noms, na.rm=TRUE)/(nomissingdonors*2);
#filter by >95% or <5%
lowafind = which(AF>0.95 | AF<0.05);
lowafsnps = colnames(SNPs.noms)[lowafind];
#save list of low af snps
write.table(lowafsnps,file=flowafsnps,quote=FALSE,row.names=FALSE,col.names=FALSE);
#save .map of no lowaf filtered SNPs
maplowaf = map.noms[-lowafind,];
write.table(maplowaf,file=flowafmap,quote=FALSE,row.names=FALSE,col.names=FALSE);
#make matrix for no lowaf filtered .ped 
SNPs.lowmaf = SNPs.noms[,-lowafind];
pedSNPs.lowmaf = matrix(NA,nrow=ndonors, ncol=(2*ncol(SNPs.lowmaf)));
for (i in 1:ncol(SNPs.lowmaf)) { 
  NAind = which(is.na(SNPs.lowmaf[,i]));
  hom1ind = which(SNPs.lowmaf[,i]==0);
}
#save no lowaf snps .ped
ped.lowmaf = cbind(ped[,1:6],pedSNPs.lowmaf);
write.table(ped.lowmaf,file=flowafped,quote=FALSE,row.names=FALSE,col.names=FALSE);
phe = read.table(fphe)$V3; #read in phenotype data

pdf(fpdf);
#plot phenotype against first 100 SNP genotype w/o low freq
for (i in 1:100){
  tmp = data.frame(phenotype=phe,SNPs.lowmaf[,i]); #make df for prob 3 plots
  colnames(tmp)[2] = "SNP";
  thisplot = ggplot(data=tmp)+
    geom_boxplot(mapping=aes(x=SNP,y=phenotype,group=SNP)) +
    geom_jitter(mapping=aes(x=SNP,y=phenotype),width=0.10,height=0)+
    labs (title=colnames(SNPs.lowmaf)[i]);
  print(thisplot);
}
dev.off()

